+++
title = "ITcracy"
description = "Automation for everyone"

+++

## A team specializing in

- Building Websites
- Process Automation
- Sharing Knowledge

### Using Python and other sophisticated **Free and Open Source Software**
